package com.lluiscriado.tindermdpa.welcomeTest;

import android.support.test.InstrumentationRegistry;
import android.support.test.filters.MediumTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import com.lluiscriado.tindermdpa.R;
import com.lluiscriado.tindermdpa.TinderApplication;
import com.lluiscriado.tindermdpa.manager.WelcomeManager;
import com.lluiscriado.tindermdpa.views.WelcomeActivity;
import com.lluiscriado.tindermdpa.views.fragments.WelcomeLoginFragment;
import com.lluiscriado.tindermdpa.views.fragments.WelcomeRegisterFragment;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import toothpick.Scope;
import toothpick.Toothpick;
import toothpick.config.Module;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.instanceOf;
import static org.junit.Assert.assertThat;

/**
 * Created by lluis.criado on 06/03/2018.
 */

@RunWith(AndroidJUnit4.class)
@MediumTest
public class WelcomeActivityTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Rule
    public ActivityTestRule<WelcomeActivity> welcomeActivityRule = new ActivityTestRule<>(WelcomeActivity.class, false, false);

    @Mock
    private static WelcomeManager welcomeManagerMock;

    private static TinderApplication application;
    private static Scope applicationScope;

    @Before
    public void setup() {
        application = (TinderApplication) InstrumentationRegistry.getTargetContext().getApplicationContext();
        applicationScope = Toothpick.openScope(application);

        // We need to install the modules and launch the activity manually
        applicationScope.installTestModules(new Module() {{
            bind(WelcomeManager.class).toInstance(welcomeManagerMock);
        }});

        welcomeActivityRule.launchActivity(null);
    }

    @After
    public void tearDown() {
        Toothpick.reset(applicationScope);
        application.installToothPickModules(applicationScope);
    }

    @Test
    public void checkFirstFragmentLoadedIsLogin() {
        FragmentManager fragment = welcomeActivityRule.getActivity().getSupportFragmentManager();
        Fragment currentFragment = fragment.findFragmentById(R.id.fragment_layout);
        assertThat(currentFragment, instanceOf(WelcomeLoginFragment.class));
    }

    @Test
    public void checkSwitchFragments() {
        onView(withId(R.id.switchFragments)).perform(click());
        FragmentManager fragment = welcomeActivityRule.getActivity().getSupportFragmentManager();
        Fragment currentFragment = fragment.findFragmentById(R.id.fragment_layout);
        assertThat(currentFragment, instanceOf(WelcomeRegisterFragment.class));
    }
}
