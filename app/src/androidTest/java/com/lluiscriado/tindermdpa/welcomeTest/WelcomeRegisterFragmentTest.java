package com.lluiscriado.tindermdpa.welcomeTest;

import android.support.test.InstrumentationRegistry;
import android.support.test.filters.MediumTest;
import android.support.test.runner.AndroidJUnit4;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

import com.android21buttons.fragmenttestrule.FragmentTestRule;
import com.lluiscriado.tindermdpa.R;
import com.lluiscriado.tindermdpa.TinderApplication;
import com.lluiscriado.tindermdpa.manager.WelcomeManager;
import com.lluiscriado.tindermdpa.views.fragments.WelcomeRegisterFragment;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import toothpick.Scope;
import toothpick.Toothpick;
import toothpick.config.Module;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.withId;


/**
 * Created by lluis.criado on 06/03/2018.
 */

@RunWith(AndroidJUnit4.class)
@MediumTest
public class WelcomeRegisterFragmentTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Rule
    public FragmentTestRule<?, WelcomeRegisterFragment> welcomeRegisterFragmentTestRule = FragmentTestRule.create(WelcomeRegisterFragment.class, false, false);

    @Mock
    private WelcomeManager welcomeManagerMock;

    private TinderApplication application;
    private Scope applicationScope;


    @Before
    public void setup() {
        application = (TinderApplication) InstrumentationRegistry.getTargetContext().getApplicationContext();
        applicationScope = Toothpick.openScope(application);

        // We need to install the modules and launch the activity manually
        applicationScope.installTestModules(new Module() {{
            bind(WelcomeManager.class).toInstance(welcomeManagerMock);
        }});

        welcomeRegisterFragmentTestRule.launchFragment(welcomeRegisterFragmentTestRule.getFragment());
    }

    @After
    public void tearDown() {
        Toothpick.reset(applicationScope);
        application.installToothPickModules(applicationScope);
    }

    @Test
    public void checkRegisterInitialState() {
        // Check username is empty
        EditText username = welcomeRegisterFragmentTestRule.getActivity().findViewById(R.id.username);
        Assert.assertTrue(username.getText().toString().isEmpty());

        // Check password is empty
        EditText password = welcomeRegisterFragmentTestRule.getActivity().findViewById(R.id.password);
        Assert.assertTrue(password.getText().toString().isEmpty());

        // Check checkbox is not checked
        CheckBox checkBox = welcomeRegisterFragmentTestRule.getActivity().findViewById(R.id.checkBox);
        Assert.assertFalse(checkBox.isChecked());

        // Check errorMessage is not shown
        TextView errorMessage = welcomeRegisterFragmentTestRule.getActivity().findViewById(R.id.textViewErrorMessage);
        Assert.assertEquals(0, errorMessage.getAlpha(), 0);
    }

    @Test
    public void checkRegisterWithEmptyUserFails() {
        onView(withId(R.id.password)).perform(typeText("123456"), closeSoftKeyboard());
        onView(withId(R.id.checkBox)).perform(click());
        onView(withId(R.id.registerButton)).perform(click());

        // Check errorMessage is shown
        TextView errorMessage = welcomeRegisterFragmentTestRule.getActivity().findViewById(R.id.textViewErrorMessage);
        Assert.assertEquals(1, errorMessage.getAlpha(), 0);
    }

    @Test
    public void checkRegisterWithEmptyPasswordFails() {
        onView(withId(R.id.username)).perform(typeText("Lluis Criado"), closeSoftKeyboard());
        onView(withId(R.id.checkBox)).perform(click());
        onView(withId(R.id.registerButton)).perform(click());

        // Check errorMessage is shown
        TextView errorMessage = welcomeRegisterFragmentTestRule.getActivity().findViewById(R.id.textViewErrorMessage);
        Assert.assertEquals(1, errorMessage.getAlpha(), 0);
    }

    @Test
    public void checkRegisterWithoutCheckboxFails() {
        onView(withId(R.id.username)).perform(typeText("Lluis Criado"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(typeText("123456"), closeSoftKeyboard());
        onView(withId(R.id.registerButton)).perform(click());

        // Check errorMessage is shown
        TextView errorMessage = welcomeRegisterFragmentTestRule.getActivity().findViewById(R.id.textViewErrorMessage);
        Assert.assertEquals(1, errorMessage.getAlpha(), 0);
    }

    @Test
    public void checkRegisterSuccess() {
        onView(withId(R.id.username)).perform(typeText("Lluis Criado"), closeSoftKeyboard());
        onView(withId(R.id.password)).perform(typeText("123456"), closeSoftKeyboard());
        onView(withId(R.id.checkBox)).perform(click());
        onView(withId(R.id.registerButton)).perform(click());

        // Check errorMessage is shown
        TextView errorMessage = welcomeRegisterFragmentTestRule.getActivity().findViewById(R.id.textViewErrorMessage);
        Assert.assertEquals(0, errorMessage.getAlpha(), 0);
    }
}
