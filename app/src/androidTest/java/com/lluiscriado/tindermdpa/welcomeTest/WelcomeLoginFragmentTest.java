package com.lluiscriado.tindermdpa.welcomeTest;

import android.support.test.InstrumentationRegistry;
import android.support.test.filters.MediumTest;
import android.support.test.runner.AndroidJUnit4;
import android.widget.EditText;

import com.android21buttons.fragmenttestrule.FragmentTestRule;
import com.lluiscriado.tindermdpa.R;
import com.lluiscriado.tindermdpa.TinderApplication;
import com.lluiscriado.tindermdpa.manager.WelcomeManager;
import com.lluiscriado.tindermdpa.views.fragments.WelcomeLoginFragment;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import toothpick.Scope;
import toothpick.Toothpick;
import toothpick.config.Module;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.RootMatchers.withDecorView;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.core.IsNot.not;

/**
 * Created by lluis.criado on 06/03/2018.
 */

@RunWith(AndroidJUnit4.class)
@MediumTest
public class WelcomeLoginFragmentTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Rule
    public FragmentTestRule<?, WelcomeLoginFragment> welcomeLoginFragmentTestRule = FragmentTestRule.create(WelcomeLoginFragment.class, false, false);

    @Mock
    private WelcomeManager welcomeManagerMock;

    private TinderApplication application;
    private Scope applicationScope;


    @Before
    public void setup() {
        application = (TinderApplication) InstrumentationRegistry.getTargetContext().getApplicationContext();
        applicationScope = Toothpick.openScope(application);

        // We need to install the modules and launch the activity manually
        applicationScope.installTestModules(new Module() {{
            bind(WelcomeManager.class).toInstance(welcomeManagerMock);
        }});

        welcomeLoginFragmentTestRule.launchFragment(welcomeLoginFragmentTestRule.getFragment());
    }

    @After
    public void tearDown() {
        Toothpick.reset(applicationScope);
        application.installToothPickModules(applicationScope);
    }

    @Test
    public void checkLoginInitialState() {
        // Check username is empty
        EditText username = welcomeLoginFragmentTestRule.getActivity().findViewById(R.id.username);
        Assert.assertTrue(username.getText().toString().isEmpty());

        // Check password is empty
        EditText password = welcomeLoginFragmentTestRule.getActivity().findViewById(R.id.password);
        Assert.assertTrue(password.getText().toString().isEmpty());
    }

    @Test
    public void checkEmptyUsernameLoginFails() {
        onView(withId(R.id.password)).perform(typeText("123456"), closeSoftKeyboard());
        onView(withId(R.id.loginButton)).perform(click());

        // Check that a toast is displayed because of empty user and password
        onView(withText("Username and password must be filled")).
                inRoot(withDecorView(not(is(welcomeLoginFragmentTestRule.getActivity().getWindow().getDecorView())))).
                check(matches(isDisplayed()));
    }

    @Test
    public void checkEmptyPasswordLoginFails() {
        onView(withId(R.id.username)).perform(typeText("Lluis Criado"), closeSoftKeyboard());
        onView(withId(R.id.loginButton)).perform(click());

        // Check that a toast is displayed because of empty user and password
        onView(withText("Username and password must be filled")).
                inRoot(withDecorView(not(is(welcomeLoginFragmentTestRule.getActivity().getWindow().getDecorView())))).
                check(matches(isDisplayed()));
    }
}
