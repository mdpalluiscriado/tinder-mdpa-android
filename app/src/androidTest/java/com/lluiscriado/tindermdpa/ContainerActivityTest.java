package com.lluiscriado.tindermdpa;

import android.support.test.InstrumentationRegistry;
import android.support.test.filters.MediumTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.support.v4.view.ViewPager;

import com.lluiscriado.tindermdpa.manager.ConversationsManager;
import com.lluiscriado.tindermdpa.models.User;
import com.lluiscriado.tindermdpa.views.ContainerActivity;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnit;
import org.mockito.junit.MockitoRule;

import toothpick.Scope;
import toothpick.Toothpick;
import toothpick.config.Module;

import static org.mockito.Mockito.when;

/**
 * Created by lluis.criado on 06/03/2018.
 */

@RunWith(AndroidJUnit4.class)
@MediumTest
public class ContainerActivityTest {

    @Rule
    public MockitoRule rule = MockitoJUnit.rule();

    @Rule
    public ActivityTestRule<ContainerActivity> containerActivityActivityTestRule = new ActivityTestRule<>(ContainerActivity.class, false, false);

    @Mock
    private static ConversationsManager conversationsManagerMock;

    private static TinderApplication application;
    private static Scope applicationScope;


    @Before
    public void setup() {
        application = (TinderApplication) InstrumentationRegistry.getTargetContext().getApplicationContext();
        applicationScope = Toothpick.openScope(application);

        // We need to install the modules and launch the activity manually
        applicationScope.installTestModules(new Module() {{
            bind(ConversationsManager.class).toInstance(conversationsManagerMock);
        }});


        User mockedUser = TestManager.loggedUser();
        when(conversationsManagerMock.getLoggedUser()).thenReturn(mockedUser);

        containerActivityActivityTestRule.launchActivity(null);
    }

    @After
    public void tearDown() {
        Toothpick.reset(applicationScope);
        application.installToothPickModules(applicationScope);
    }

    @Test
    public void checkViewPagerIsCreated() {
        ViewPager viewPager = containerActivityActivityTestRule.getActivity().findViewById(R.id.pager);
        Assert.assertEquals(3, viewPager.getAdapter().getCount());
    }
}
