package com.lluiscriado.tindermdpa;

import com.lluiscriado.tindermdpa.models.User;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by lluis on 11/03/2018.
 */

public class TestManager {

    public static User loggedUser() {
        JSONObject jsonObject;
        try {
            jsonObject = new JSONObject("{\n" +
                    "  \"userId\": \"10\",\n" +
                    "  \"username\": \"Leo Messi\",\n" +
                    "  \"password\": \"$2a$08$OXEwWxP5M82IIoHHZfipRO..w/07Q6UpWeHzQCx5H7o8oPJi/3Eta\",\n" +
                    "  \"email\": \"leo.messi@gmail.com\",\n" +
                    "  \"isMale\": true,\n" +
                    "  \"description\": \"I am a football Player\",\n" +
                    "  \"currentJob\": \"Football player\",\n" +
                    "  \"university\": \"FC Barcelona\",\n" +
                    "  \"matches\": [ \"3\", \"5\", \"12\", \"20\"],\n" +
                    "  \"conversations\": [ \"conversationId1\",  \"conversationId2\", \"conversationId3\"],\n" +
                    "  \"ageRangePreference\": { \"from\": 20, \"to\": 25 },\n" +
                    "  \"profilePicLink\": \"https:media-public.fcbarcelona.com/20157/0/document_thumbnail/20197/174/102/254/50226862/1.0-7/50226862.jpg?t=1500558216000\",\n" +
                    "  \"usersSeen\": [\n" +
                    "    {\n" +
                    "      \"userSeenId\": \"3\",\n" +
                    "      \"isLiked\": true\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"userSeenId\": \"4\",\n" +
                    "      \"isLiked\": true\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"userSeenId\": \"5\",\n" +
                    "      \"isLiked\": true\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"userSeenId\": \"12\",\n" +
                    "      \"isLiked\": true\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"userSeenId\": \"14\",\n" +
                    "      \"isLiked\": false\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"userSeenId\": \"17\",\n" +
                    "      \"isLiked\": true\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"userSeenId\": \"19\",\n" +
                    "      \"isLiked\": false\n" +
                    "    },\n" +
                    "    {\n" +
                    "      \"userSeenId\": \"20\",\n" +
                    "      \"isLiked\": true\n" +
                    "    }\n" +
                    "  ],\n" +
                    "  \"images\": [\n" +
                    "    \"https:media-public.fcbarcelona.com/20157/0/document_thumbnail/20197/174/102/254/50226862/1.0-7/50226862.jpg?t=1500558216000\",\n" +
                    "    \"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTX0mavbLkb6tIXREU_yAuvm4O0RFPv-URdXAvugF9e4G6FHTSOtg\",\n" +
                    "    \"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRzMP5q4aUc28KL3Ii5eRpa5isFnFmLtzDDVnKoDp3wmC_Laj5HdA\",\n" +
                    "    \"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQgVpGiw_7N8gNwqDgYsvk4PH4tR6nvv4Hq0XF3MzqAY-tUJxkawQ\",\n" +
                    "    \"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcTDc5Kkza1wxrUcquvMky4sYOfAnQUzEzrMM266tHkIqp0s360r\",\n" +
                    "    \"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRKUeGMx2EGirUQSqJ51fzHXZmbPkOTxQEFuSym6oGpFElINeyy\"\n" +
                    "  ]\n" +
                    "}");
        } catch (JSONException e) {
            e.printStackTrace();
            jsonObject = null;
        }
        return new User(jsonObject);
    }
}
