package com.lluiscriado.tindermdpa.models;

import android.location.Location;

import com.lluiscriado.tindermdpa.utils.enums.MessageOwner;
import com.lluiscriado.tindermdpa.utils.enums.MessageType;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.StringTokenizer;

/**
 * Created by lluis.criado on 31/01/2018.
 */

public class Message {

    public MessageOwner owner;
    public MessageType type;
    public Object content;
    public Long timeStamp;
    public Boolean isRead;
    public String toId;
    public String fromId;

    // Constructors
    // =============================================================================================
    public Message(MessageType type, Object content, String fromId, String toId) {
        this.type = type;
        this.timeStamp = System.currentTimeMillis()/1000;
        this.content = content;
        this.isRead = false;
        this.fromId = fromId;
        this.toId = toId;
    }

    public Message(JSONObject messageJSON){
        try {
            String typeString = messageJSON.getString("type");
            MessageType type = MessageType.valueOf(typeString);

            Long timeStamp  = messageJSON.getLong("timestamp");
            Object content  = messageJSON.get("content");
            Boolean isRead  = messageJSON.getBoolean("isRead");
            String toID     = messageJSON.getString("toId");
            String fromID   = messageJSON.getString("fromId");

            if ( type == null || content == null || isRead == null || toID == null || fromID == null )
                throw new IllegalArgumentException();

            this.type = type;
            this.timeStamp = timeStamp;
            this.content = content;
            this.isRead = isRead;
            this.toId = toID;
            this.fromId = fromID;

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // Public methods
    // =============================================================================================
    public String getContentString() {
        switch (type) {
            case photo:
                return "Media";
            case text:
                return (String)content;
            case location:
                return "Location";
            default:
                return "";
        }
    }

    public Location getMessageLocation() {
        Location location = null;

        if ( type == MessageType.location ){
            StringTokenizer tokens = new StringTokenizer((String)content, ":");
            try {
                double latitude = Double.parseDouble( tokens.nextToken() );
                double longitude = Double.parseDouble( tokens.nextToken() );
                location = new Location("");
                location.setLatitude(latitude);
                location.setLongitude(longitude);
            }catch (NumberFormatException e) {
                //Do nothing
            }
        }
        return location;
    }

    public JSONObject toJSON() {
        JSONObject messageJSON = new JSONObject();

        try {
            messageJSON.put("content", content);
            messageJSON.put("fromId", fromId);
            messageJSON.put("isRead", isRead);
            messageJSON.put("timestamp", timeStamp);
            messageJSON.put("toId", toId);
            messageJSON.put("type", type.toString());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return messageJSON;
    }
}
