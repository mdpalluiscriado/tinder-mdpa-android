package com.lluiscriado.tindermdpa.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static com.lluiscriado.tindermdpa.utils.Utils.jsonArrayToMessageList;
import static com.lluiscriado.tindermdpa.utils.Utils.jsonArrayToUserList;

/**
 * Created by lluis.criado on 31/01/2018.
 */

public class Conversation {

    public String id;
    public List<User> users;
    public User user;
    public List<Message> messages;
    public Message lastMessage;

    // Constructors
    // =============================================================================================
    public Conversation(String id, User user, Message lastMessage ) {
        if ( user == null)
            throw new IllegalArgumentException();

        this.user = user;
        this.lastMessage = lastMessage;
        this.id = id;
    }

    public Conversation(JSONObject conversationJSON) {
        try {
            this.id = conversationJSON.getString("conversationId");
            JSONArray usersJSON = conversationJSON.getJSONArray("users");
            this.users = jsonArrayToUserList(usersJSON);
            JSONArray messages = conversationJSON.getJSONArray("messages");
            this.messages = jsonArrayToMessageList(messages);
            JSONObject lastMessageJSON = messages.getJSONObject(messages.length() - 1);
            this.lastMessage = new Message(lastMessageJSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

}