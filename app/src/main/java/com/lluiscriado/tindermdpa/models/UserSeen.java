package com.lluiscriado.tindermdpa.models;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by lluis on 30/03/2018.
 */

public class UserSeen {

    public String userSeenId;
    public boolean isLiked;

    // Constructors
    // =============================================================================================
    public UserSeen(JSONObject userSeenJson) {
        try {
            userSeenId  = userSeenJson.getString("userSeenId");
            isLiked = userSeenJson.getBoolean("isLiked");
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }
}
