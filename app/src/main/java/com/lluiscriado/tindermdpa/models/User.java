package com.lluiscriado.tindermdpa.models;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import static com.lluiscriado.tindermdpa.utils.Utils.jsonArrayToStringList;
import static com.lluiscriado.tindermdpa.utils.Utils.jsonArrayToUserSeenList;

/**
 * Created by lluis.criado on 29/01/2018.
 */

public class User {

    public String username;
    public String userId;
    public String profilePicLink;
    public String password;
    public String email;
    public boolean isMale;
    public String description;
    public String currentJob;
    public String university;
    public int fromAge;
    public int toAge;
    public List<String> conversations;
    public List<String> matches;
    public List<UserSeen> usersSeen;


    // Constructors
    // =============================================================================================
    public User(String username, String userId, String profilePicLink) {
        this.userId         = userId;
        this.username       = username;
        this.profilePicLink = profilePicLink;
    }

    public User(JSONObject userJSON) {
        try {
            userId  = userJSON.getString("userId");
            username = userJSON.getString("username");
            profilePicLink = userJSON.getString("profilePicLink");
            password = userJSON.getString("password");
            email = userJSON.getString("email");
            description = userJSON.getString("description");
            currentJob = userJSON.getString("currentJob");
            university = userJSON.getString("university");
            isMale = userJSON.getBoolean("isMale");
            JSONArray usersSeen = userJSON.getJSONArray("usersSeen");
            this.usersSeen = jsonArrayToUserSeenList(usersSeen);
            JSONArray conversations = userJSON.getJSONArray("conversations");
            this.conversations = jsonArrayToStringList(conversations);
            JSONArray matches = userJSON.getJSONArray("matches");
            this.matches = jsonArrayToStringList(matches);
            JSONObject ageRangePreferenceJSON = userJSON.getJSONObject("ageRangePreference");
            fromAge = ageRangePreferenceJSON.getInt("from");
            toAge = ageRangePreferenceJSON.getInt("to");

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    // Public methods
    // =============================================================================================

    public JSONObject toJSON() {
        JSONObject userJSON = new JSONObject();

        try {
            userJSON.put("username", username);
            userJSON.put("userId", userId);
            userJSON.put("profilePicLink", profilePicLink);
            userJSON.put("email", email);
            userJSON.put("description", description);
            userJSON.put("currentJob", currentJob);
            userJSON.put("university", university);
            userJSON.put("conversations", conversations);
            userJSON.put("matches", matches);

            JSONObject ageRangePreferenceJSON = new JSONObject();
            ageRangePreferenceJSON.put("from", fromAge);
            ageRangePreferenceJSON.put("to", toAge);
            userJSON.put("ageRangePreference", ageRangePreferenceJSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return userJSON;
    }

    public JSONObject toConversationJSON() {
        JSONObject userJSON = new JSONObject();

        try {
            userJSON.put("username", username);
            userJSON.put("userId", userId);
            userJSON.put("profilePicLink", profilePicLink);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return userJSON;
    }
}
