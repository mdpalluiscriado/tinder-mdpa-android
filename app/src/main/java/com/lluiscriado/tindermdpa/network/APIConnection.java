package com.lluiscriado.tindermdpa.network;

import com.lluiscriado.tindermdpa.listeners.APIConnectionListener;
import com.lluiscriado.tindermdpa.utils.enums.RESTMethod;

import org.json.JSONObject;

import javax.inject.Singleton;

/**
 * Created by lluis.criado on 26/03/2018.
 */

@Singleton
public class APIConnection {

    private static String baseUrl = "https://mdpalluiscriado.azurewebsites.net/";  // Server
    //private static String baseUrl = "http://192.168.1.104:3000/";                           // Local

    private APIConnection() { }

    public static void get(String url, JSONObject parameters, final APIConnectionListener onResponse) {
        new APIConnectionTask(RESTMethod.GET, baseUrl + url, parameters, onResponse).execute();
    }

    public static void post(String url, JSONObject parameters, final APIConnectionListener onResponse) {
        new APIConnectionTask(RESTMethod.POST, baseUrl + url, parameters, onResponse).execute();
    }
}
