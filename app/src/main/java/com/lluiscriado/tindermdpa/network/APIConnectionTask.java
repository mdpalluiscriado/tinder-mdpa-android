package com.lluiscriado.tindermdpa.network;

import android.os.AsyncTask;

import com.lluiscriado.tindermdpa.listeners.APIConnectionListener;
import com.lluiscriado.tindermdpa.utils.enums.RESTMethod;

import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;

/**
 * Created by lluis.criado on 26/03/2018.
 */

public class APIConnectionTask extends AsyncTask<Void, Void, Void> {

    private RESTMethod method;
    private String url;
    private JSONObject parameters;

    private APIConnectionListener response = new APIConnectionListener() {
        @Override
        public void onAPIConnectionResponseOK(String response) {
        }

        @Override
        public void onAPIConnectionResponseError(String error) {
        }
    };


    public APIConnectionTask(RESTMethod method, String url, JSONObject parameters, APIConnectionListener response) {
        this.method = method;
        this.url = url;
        this.parameters = parameters;
        this.response =  response;
    }

    ////--------------- Private methods ---------------\\\\
    @Override
    protected Void doInBackground(Void... params) {
        request(method, url, parameters);
        return null;
    }

    private void request(RESTMethod method, String url, JSONObject params) {
        if (method == RESTMethod.GET)
            get(url);
        else if (method == RESTMethod.POST)
            post(url, params);
    }

    private HttpURLConnection createServerConnection(String connectionType, String url) throws IOException {
        URL urlFormat = new URL(url);
        HttpURLConnection urlConnection = (HttpURLConnection) urlFormat.openConnection();
        urlConnection.setReadTimeout(30000);
        urlConnection.setConnectTimeout(30000);
        urlConnection.setRequestMethod(connectionType);
        return urlConnection;
    }

    private void get(String url) {
        HttpURLConnection urlConnection = null;
        try {
            urlConnection = createServerConnection("GET", url);
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            String response = readInputStream(in);
            this.response.onAPIConnectionResponseOK(response);
        } catch (Exception e) {
            e.printStackTrace();
            this.response.onAPIConnectionResponseError(e.toString());
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
    }

    private void post(String url, JSONObject params) {
        StringBuilder response = new StringBuilder();
        HttpURLConnection urlConnection = null;
        try {
            urlConnection = createServerConnection("POST", url);
            urlConnection.setDoOutput(true);
            urlConnection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");

            OutputStream os = urlConnection.getOutputStream();
            BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "UTF-8"));
            writer.write(params.toString());
            writer.flush();
            writer.close();
            os.close();
            String line;
            long responseCode = urlConnection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK) {
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
                while ((line = br.readLine()) != null) {
                    response.append(line);
                }
                this.response.onAPIConnectionResponseOK(response.toString());
            } else if (urlConnection.getErrorStream() != null) {
                BufferedReader br = new BufferedReader(new InputStreamReader(urlConnection.getErrorStream()));
                while ((line = br.readLine()) != null) {
                    response.append(line);
                }
                this.response.onAPIConnectionResponseError("Response Code = " + responseCode + " Response error: " + response);
            }
        } catch (Exception e) {
            e.printStackTrace();
            this.response.onAPIConnectionResponseError(e.toString());
        } finally {
            if (urlConnection != null)
                urlConnection.disconnect();
        }
    }

    private String readInputStream(InputStream in) {
        String inputStr = null;
        try {
            BufferedReader streamReader = new BufferedReader(new InputStreamReader(in, "UTF-8"));
            StringBuilder responseStrBuilder = new StringBuilder();
            while ((inputStr = streamReader.readLine()) != null) {
                responseStrBuilder.append(inputStr);
            }
            inputStr = responseStrBuilder.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return inputStr;
    }
}