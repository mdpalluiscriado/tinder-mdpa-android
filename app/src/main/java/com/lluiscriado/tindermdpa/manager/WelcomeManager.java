package com.lluiscriado.tindermdpa.manager;

import com.lluiscriado.tindermdpa.listeners.LoginUserListener;
import com.lluiscriado.tindermdpa.listeners.RegisterUserListener;

/**
 * Created by lluis.criado on 01/03/2018.
 */

public interface WelcomeManager {

    void loginUser(String username, String password, LoginUserListener loginUserListener);

    void registerUser(String username, String password, RegisterUserListener registerUserListener);
}
