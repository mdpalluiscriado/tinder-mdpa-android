package com.lluiscriado.tindermdpa.manager;

import com.lluiscriado.tindermdpa.listeners.GetConversationsListener;
import com.lluiscriado.tindermdpa.listeners.GetMatchesListener;

import javax.inject.Singleton;

/**
 * Created by lluis.criado on 08/03/2018.
 */

@Singleton
public interface ConversationsManager {

    void getConversationList(GetConversationsListener getConversationsListener);

    void getMatchList(GetMatchesListener getMatchesListener);
}