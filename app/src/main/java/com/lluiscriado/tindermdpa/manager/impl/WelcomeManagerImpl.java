package com.lluiscriado.tindermdpa.manager.impl;

import com.lluiscriado.tindermdpa.listeners.LoginUserListener;
import com.lluiscriado.tindermdpa.listeners.RegisterUserListener;
import com.lluiscriado.tindermdpa.manager.TinderManager;
import com.lluiscriado.tindermdpa.manager.WelcomeManager;
import com.lluiscriado.tindermdpa.models.User;
import com.lluiscriado.tindermdpa.listeners.APIConnectionListener;
import com.lluiscriado.tindermdpa.network.APIConnection;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by lluis.criado on 01/03/2018.
 */

@Singleton
public class WelcomeManagerImpl implements WelcomeManager {

    // Dependency injection
    // =============================================================================================
    @Inject TinderManager tinderManager;


    // Welcome manager methods
    // =============================================================================================
    @Override
    public void loginUser(String username, String password, final LoginUserListener loginUserListener) {
        APIConnection.post("login", createRequestBody(username, password), new APIConnectionListener() {
            @Override
            public void onAPIConnectionResponseOK(String response) {
                User loggedUser = getLoggedUserFromResponse(response);
                tinderManager.setLoggedUser(loggedUser);
                loginUserListener.onLoginSuccess();
            }

            @Override
            public void onAPIConnectionResponseError(String error) {
                loginUserListener.onLoginError(error);
            }
        });
    }

    @Override
    public void registerUser(String username, String password, final RegisterUserListener registerUserListener) {
        APIConnection.post("signup", createRequestBody(username, password), new APIConnectionListener() {
            @Override
            public void onAPIConnectionResponseOK(String response) {
                User loggedUser = getLoggedUserFromResponse(response);
                tinderManager.setLoggedUser(loggedUser);
                registerUserListener.onRegisterSuccess();
            }

            @Override
            public void onAPIConnectionResponseError(String error) {
                registerUserListener.onRegisterError(error);
            }
        });
    }


    // Private methods
    // =============================================================================================
    private JSONObject createRequestBody(String username, String password) {
        JSONObject requestBody = new JSONObject();

        try {
            requestBody.put("username", username);
            requestBody.put("password", password);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return requestBody;
    }

    private User getLoggedUserFromResponse(String response) {
        User user;
        try {
            JSONObject jsonObject = new JSONObject(response);
            JSONObject userJSON = jsonObject.getJSONObject("user");
            user = new User(userJSON);
        } catch (JSONException e) {
            e.printStackTrace();
            user = null;
        }

        return user;
    }
}

