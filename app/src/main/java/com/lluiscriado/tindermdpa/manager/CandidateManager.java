package com.lluiscriado.tindermdpa.manager;

import com.lluiscriado.tindermdpa.listeners.GetCandidatesListener;
import com.lluiscriado.tindermdpa.listeners.NewUserSeenListener;

/**
 * Created by lluis.criado on 05/02/2018.
 */

public interface CandidateManager {

    void getCandidateList(GetCandidatesListener getCandidatesListener);

    void newUserSeen(String userSeenId, boolean isLiked, NewUserSeenListener newUserSeenListener);
}