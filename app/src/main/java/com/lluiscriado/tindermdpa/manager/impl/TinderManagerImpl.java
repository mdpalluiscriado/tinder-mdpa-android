package com.lluiscriado.tindermdpa.manager.impl;

import com.lluiscriado.tindermdpa.manager.TinderManager;
import com.lluiscriado.tindermdpa.models.User;

import javax.inject.Singleton;

/**
 * Created by lluis.criado on 26/03/2018.
 */

@Singleton
public class TinderManagerImpl implements TinderManager {

    private User loggedUser;

    // Public methods
    // =============================================================================================
    @Override
    public User getLoggedUser() {
        return loggedUser;
    }

    @Override
    public void setLoggedUser(User loggedUser) {
        this.loggedUser = loggedUser;
    }
}
