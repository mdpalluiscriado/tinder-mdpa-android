package com.lluiscriado.tindermdpa.manager.impl;

import android.util.Log;

import com.lluiscriado.tindermdpa.listeners.APIConnectionListener;
import com.lluiscriado.tindermdpa.listeners.GetCandidatesListener;
import com.lluiscriado.tindermdpa.listeners.NewUserSeenListener;
import com.lluiscriado.tindermdpa.manager.CandidateManager;
import com.lluiscriado.tindermdpa.manager.TinderManager;
import com.lluiscriado.tindermdpa.models.User;
import com.lluiscriado.tindermdpa.network.APIConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.lluiscriado.tindermdpa.utils.Utils.jsonArrayToUserList;

/**
 * Created by lluis.criado on 26/03/2018.
 */

@Singleton
public class CandidateManagerImpl implements CandidateManager {

    @Inject TinderManager tinderManager;

    // Public methods
    // =============================================================================================
    @Override
    public void getCandidateList(GetCandidatesListener getCandidatesListener) {

        APIConnection.get("users", null, new APIConnectionListener() {
            @Override
            public void onAPIConnectionResponseOK(String response) {
                getCandidatesListener.onGetCandidatesSuccess(getCandidateListFromResponse(response));
            }

            @Override
            public void onAPIConnectionResponseError(String error) {
                Log.i("error", error);
                getCandidatesListener.onGetCandidatesError(error);
            }
        });
    }

    @Override
    public void newUserSeen(String userSeenId, boolean isLiked, NewUserSeenListener newUserSeenListener) {
        String url = "users/" + tinderManager.getLoggedUser().userId + "/usersSeen";
        JSONObject jsonObject = createRequestBody(userSeenId, isLiked);

        APIConnection.post(url, jsonObject, new APIConnectionListener() {
            @Override
            public void onAPIConnectionResponseOK(String response) {
                newUserSeenListener.onNewUserSeenRequestSuccess(checkMatchFromResponse(response));
            }

            @Override
            public void onAPIConnectionResponseError(String error) {
                newUserSeenListener.onNewUserSeenRequestError(error);
            }
        });
    }


    // Private methods
    // =============================================================================================
    private List<User> getCandidateListFromResponse(String response) {
        List<User> candidateList = new ArrayList<>();

        try {
            candidateList = jsonArrayToUserList(new JSONArray(response));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return candidateList;
    }

    private JSONObject createRequestBody(String userSeenId, boolean isLiked) {
        JSONObject requestBody = new JSONObject();

        try {
            requestBody.put("userSeenId", userSeenId);
            requestBody.put("isLiked", isLiked);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return requestBody;
    }

    private boolean checkMatchFromResponse(String response) {
        boolean isMatch = false;
        try {
            JSONObject jsonObject = new JSONObject(response);
            isMatch = jsonObject.getBoolean("match");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return isMatch;
    }
}
