package com.lluiscriado.tindermdpa.manager.impl;

import com.lluiscriado.tindermdpa.listeners.APIConnectionListener;
import com.lluiscriado.tindermdpa.listeners.UpdateValuesLoggedUserListener;
import com.lluiscriado.tindermdpa.manager.SettingsManager;
import com.lluiscriado.tindermdpa.manager.TinderManager;
import com.lluiscriado.tindermdpa.models.User;
import com.lluiscriado.tindermdpa.network.APIConnection;

import org.json.JSONException;
import org.json.JSONObject;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by lluis.criado on 26/03/2018.
 */

@Singleton
public class SettingsManagerImpl implements SettingsManager {

    @Inject TinderManager tinderManager;

    // Public methods
    // =============================================================================================
    @Override
    public void updateValues(User newValuesForLoggedUser, UpdateValuesLoggedUserListener updateValuesLoggedUserListener) {
        APIConnection.post("users/" + newValuesForLoggedUser.userId, newValuesForLoggedUser.toJSON(), new APIConnectionListener() {
            @Override
            public void onAPIConnectionResponseOK(String response) {
                tinderManager.setLoggedUser(getUserFromResponse(response));
                updateValuesLoggedUserListener.onUpdateValuesLoggedUserSuccess();
            }

            @Override
            public void onAPIConnectionResponseError(String error) {
                updateValuesLoggedUserListener.onUpdateValuesLoggedUserError(error);
            }
        });
    }

    // Private methods
    // =============================================================================================
    private User getUserFromResponse(String response) {
        User updatedUser = null;
        try {
            JSONObject userJSON = new JSONObject(response);
            updatedUser = new User(userJSON);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return updatedUser;
    }
}
