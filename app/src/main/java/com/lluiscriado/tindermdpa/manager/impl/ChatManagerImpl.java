package com.lluiscriado.tindermdpa.manager.impl;

import com.lluiscriado.tindermdpa.listeners.APIConnectionListener;
import com.lluiscriado.tindermdpa.listeners.GetMessagesListener;
import com.lluiscriado.tindermdpa.listeners.SendTextMessageListener;
import com.lluiscriado.tindermdpa.manager.ChatManager;
import com.lluiscriado.tindermdpa.manager.TinderManager;
import com.lluiscriado.tindermdpa.models.Conversation;
import com.lluiscriado.tindermdpa.models.Message;
import com.lluiscriado.tindermdpa.models.User;
import com.lluiscriado.tindermdpa.network.APIConnection;
import com.lluiscriado.tindermdpa.utils.enums.MessageOwner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import static com.lluiscriado.tindermdpa.utils.Utils.jsonArrayToMessageList;

/**
 * Created by lluis on 31/03/2018.
 */

public class ChatManagerImpl implements ChatManager {

    @Inject TinderManager tinderManager;

    // Public methods
    // =============================================================================================
    @Override
    public void sendTextMessageForNewConversation(User user, Message message, SendTextMessageListener sendTextMessageListener) {
        APIConnection.post("conversations", createRequestBody(user, message), new APIConnectionListener() {
            @Override
            public void onAPIConnectionResponseOK(String response) {
                String conversationId = getConversationIdFromResponse(response);
                sendTextMessageListener.onSendTextMessageSuccess(message, conversationId);
            }

            @Override
            public void onAPIConnectionResponseError(String error) {
                sendTextMessageListener.omSendTextMessageError(error);
            }
        });
    }

    @Override
    public void sendTextMessageForExistingConversation(Conversation conversation, Message message, SendTextMessageListener sendTextMessageListener) {
        APIConnection.post("conversations/" + conversation.id + "/messages", createRequestBody(conversation.user, message), new APIConnectionListener() {
            @Override
            public void onAPIConnectionResponseOK(String response) {
                String conversationId = getConversationIdFromResponse(response);
                sendTextMessageListener.onSendTextMessageSuccess(message, conversationId);
            }

            @Override
            public void onAPIConnectionResponseError(String error) {
                sendTextMessageListener.omSendTextMessageError(error);
            }
        });
    }

    @Override
    public void getMessages(String conversationId, GetMessagesListener getMessagesListener) {
        APIConnection.get("conversations/" + conversationId + "/messages", null, new APIConnectionListener() {
            @Override
            public void onAPIConnectionResponseOK(String response) {
                getMessagesListener.onGetMessagesSuccess(getMessagesFromResponse(response));
            }

            @Override
            public void onAPIConnectionResponseError(String error) {
                getMessagesListener.onGetMessagesError(error);
            }
        });
    }


    // Private methods
    // =============================================================================================
    private String getConversationIdFromResponse(String response) {
        return response.replace("\"", "");
    }

    private JSONObject createRequestBody(User user, Message message) {
        JSONObject requestBody = new JSONObject();

        try {
            JSONArray users = new JSONArray();
            users.put(user.toConversationJSON());
            users.put(tinderManager.getLoggedUser().toConversationJSON());

            requestBody.put("users", users);
            requestBody.put("message", message.toJSON());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return requestBody;
    }

    private List<Message> getMessagesFromResponse(String response) {
        List<Message> messageList = new ArrayList<>();

        try {
            JSONArray messagesArray = new JSONArray(response);

            for (int i=0; i<messagesArray.length(); i++) {
                Message newMessage = new Message(messagesArray.getJSONObject(i));
                newMessage.owner = tinderManager.getLoggedUser().userId.equalsIgnoreCase(newMessage.fromId) ? MessageOwner.sender : MessageOwner.receiver;
                messageList.add(newMessage);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        return messageList;
    }
}
