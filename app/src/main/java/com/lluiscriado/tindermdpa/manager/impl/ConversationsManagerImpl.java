package com.lluiscriado.tindermdpa.manager.impl;

import com.lluiscriado.tindermdpa.listeners.APIConnectionListener;
import com.lluiscriado.tindermdpa.listeners.GetConversationsListener;
import com.lluiscriado.tindermdpa.listeners.GetMatchesListener;
import com.lluiscriado.tindermdpa.manager.ConversationsManager;
import com.lluiscriado.tindermdpa.manager.TinderManager;
import com.lluiscriado.tindermdpa.models.Conversation;
import com.lluiscriado.tindermdpa.models.User;
import com.lluiscriado.tindermdpa.network.APIConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import static com.lluiscriado.tindermdpa.utils.Utils.jsonArrayToUserList;

/**
 * Created by lluis.criado on 26/03/2018.
 */

@Singleton
public class ConversationsManagerImpl implements ConversationsManager {

    @Inject TinderManager tinderManager;

    // Public methods
    // =============================================================================================
    @Override
    public void getConversationList(GetConversationsListener getConversationsListener) {
        APIConnection.get("users/" + tinderManager.getLoggedUser().userId + "/conversations", null, new APIConnectionListener() {
            @Override
            public void onAPIConnectionResponseOK(String response) {
                getConversationsListener.onGetConversationsSuccess(getConversationListFromResponse(response));
            }

            @Override
            public void onAPIConnectionResponseError(String error) {
                getConversationsListener.onGetConversationsError(error);
            }
        });
    }

    @Override
    public void getMatchList(GetMatchesListener getMatchesListener) {
        String url = "users/" + tinderManager.getLoggedUser().userId + "/matches";

        APIConnection.get(url, null, new APIConnectionListener() {
            @Override
            public void onAPIConnectionResponseOK(String response) {
                getMatchesListener.onGetMatchesSuccess(getMatchListFromResponse(response));
            }

            @Override
            public void onAPIConnectionResponseError(String error) {
                getMatchesListener.onGetMatchesError(error);
            }
        });
    }


    // Private methods
    // =============================================================================================
    private List<Conversation> getConversationListFromResponse(String response) {
        List<Conversation> conversationList = new ArrayList<>();

        try {
            JSONArray jsonFromRequest = new JSONArray(response);

            for(int index = 0; index < jsonFromRequest.length(); ++index)
            {
                JSONObject conversationJSON = jsonFromRequest.getJSONObject(index);
                Conversation conversation = new Conversation(conversationJSON);
                conversation.user = getUserOf(conversation);
                conversationList.add(conversation);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return conversationList;
    }

    private List<User> getMatchListFromResponse(String response) {
        List<User> matchList = new ArrayList<>();

        try {
            matchList = jsonArrayToUserList(new JSONArray(response));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return matchList;
    }

    private User getUserOf(Conversation conversation) {
        for (User user: conversation.users) {
            if(!user.userId.equals(tinderManager.getLoggedUser().userId))
                return user;
        }
        return null;
    }

}
