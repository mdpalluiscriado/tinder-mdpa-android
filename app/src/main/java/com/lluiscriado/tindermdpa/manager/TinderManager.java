package com.lluiscriado.tindermdpa.manager;

import com.lluiscriado.tindermdpa.models.User;

/**
 * Created by lluis.criado on 26/03/2018.
 */

public interface TinderManager {

    User getLoggedUser();

    void setLoggedUser(User loggedUser);
}
