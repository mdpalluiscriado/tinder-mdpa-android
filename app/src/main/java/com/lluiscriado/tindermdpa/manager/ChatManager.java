package com.lluiscriado.tindermdpa.manager;

import com.lluiscriado.tindermdpa.listeners.GetMessagesListener;
import com.lluiscriado.tindermdpa.listeners.SendTextMessageListener;
import com.lluiscriado.tindermdpa.models.Conversation;
import com.lluiscriado.tindermdpa.models.Message;
import com.lluiscriado.tindermdpa.models.User;

/**
 * Created by lluis on 31/03/2018.
 */

public interface ChatManager {

    void sendTextMessageForNewConversation(User user, Message text, SendTextMessageListener sendTextMessageListener);

    void sendTextMessageForExistingConversation(Conversation conversation, Message text, SendTextMessageListener sendTextMessageListener);

    void getMessages(String conversationId, GetMessagesListener getMessagesListener);
}
