package com.lluiscriado.tindermdpa;

import android.app.Application;

import com.lluiscriado.tindermdpa.manager.CandidateManager;
import com.lluiscriado.tindermdpa.manager.ChatManager;
import com.lluiscriado.tindermdpa.manager.ConversationsManager;
import com.lluiscriado.tindermdpa.manager.SettingsManager;
import com.lluiscriado.tindermdpa.manager.TinderManager;
import com.lluiscriado.tindermdpa.manager.WelcomeManager;
import com.lluiscriado.tindermdpa.manager.impl.CandidateManagerImpl;
import com.lluiscriado.tindermdpa.manager.impl.ChatManagerImpl;
import com.lluiscriado.tindermdpa.manager.impl.ConversationsManagerImpl;
import com.lluiscriado.tindermdpa.manager.impl.SettingsManagerImpl;
import com.lluiscriado.tindermdpa.manager.impl.TinderManagerImpl;
import com.lluiscriado.tindermdpa.manager.impl.WelcomeManagerImpl;

import toothpick.Scope;
import toothpick.Toothpick;
import toothpick.config.Module;

/**
 * Created by lluis.criado on 05/03/2018.
 */

public class TinderApplication extends Application {

    @Override
    public void onCreate() {
        super.onCreate();

        Scope applicationScope = Toothpick.openScope(this);
        installToothPickModules(applicationScope);
    }

    public void installToothPickModules(Scope scope) {
        scope.installModules(new Module() {{
            bind(TinderManager.class).to(TinderManagerImpl.class);
            bind(WelcomeManager.class).to(WelcomeManagerImpl.class);
            bind(SettingsManager.class).to(SettingsManagerImpl.class);
            bind(CandidateManager.class).to(CandidateManagerImpl.class);
            bind(ConversationsManager.class).to(ConversationsManagerImpl.class);
            bind(ChatManager.class).to(ChatManagerImpl.class);
        }});
    }
}