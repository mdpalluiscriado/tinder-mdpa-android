package com.lluiscriado.tindermdpa.listeners;

import com.lluiscriado.tindermdpa.models.User;

import java.util.List;

/**
 * Created by lluis.criado on 27/03/2018.
 */

public interface GetCandidatesListener {

    void onGetCandidatesSuccess(List<User> candidates);

    void onGetCandidatesError(String error);
}
