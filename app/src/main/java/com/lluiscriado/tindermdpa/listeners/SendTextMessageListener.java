package com.lluiscriado.tindermdpa.listeners;

import com.lluiscriado.tindermdpa.models.Message;

/**
 * Created by lluis on 31/03/2018.
 */

public interface SendTextMessageListener {

    void onSendTextMessageSuccess(Message message, String conversationId);

    void omSendTextMessageError(String error);
}
