package com.lluiscriado.tindermdpa.listeners;

import com.lluiscriado.tindermdpa.models.User;

import java.util.List;

/**
 * Created by lluis on 12/03/2018.
 */

public interface GetMatchesListener {

    void onGetMatchesSuccess(List<User> matchesList);

    void onGetMatchesError(String error);
}
