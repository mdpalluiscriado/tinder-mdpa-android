package com.lluiscriado.tindermdpa.listeners;

import com.lluiscriado.tindermdpa.models.Message;

import java.util.List;

/**
 * Created by lluis on 01/04/2018.
 */

public interface GetMessagesListener {

    void onGetMessagesSuccess(List<Message> messageList);

    void onGetMessagesError(String error);
}
