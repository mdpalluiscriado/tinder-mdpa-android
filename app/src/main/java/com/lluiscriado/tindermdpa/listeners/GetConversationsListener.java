package com.lluiscriado.tindermdpa.listeners;

import com.lluiscriado.tindermdpa.models.Conversation;

import java.util.List;

/**
 * Created by lluis on 12/03/2018.
 */

public interface GetConversationsListener {

    void onGetConversationsSuccess(List<Conversation> conversationList);

    void onGetConversationsError(String error);
}
