package com.lluiscriado.tindermdpa.listeners;

/**
 * Created by lluis.criado on 26/03/2018.
 */

public interface APIConnectionListener {

    void onAPIConnectionResponseOK(String response);

    void onAPIConnectionResponseError(String error);
}
