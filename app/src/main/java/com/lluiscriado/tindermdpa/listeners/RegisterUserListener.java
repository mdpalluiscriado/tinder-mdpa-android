package com.lluiscriado.tindermdpa.listeners;

/**
 * Created by lluis.criado on 06/03/2018.
 */

public interface RegisterUserListener {

    void onRegisterSuccess();

    void onRegisterError(String error);
}
