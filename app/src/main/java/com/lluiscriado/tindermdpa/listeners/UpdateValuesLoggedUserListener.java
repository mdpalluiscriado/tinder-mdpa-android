package com.lluiscriado.tindermdpa.listeners;

/**
 * Created by lluis on 11/03/2018.
 */

public interface UpdateValuesLoggedUserListener {

    void onUpdateValuesLoggedUserSuccess();

    void onUpdateValuesLoggedUserError(String error);
}
