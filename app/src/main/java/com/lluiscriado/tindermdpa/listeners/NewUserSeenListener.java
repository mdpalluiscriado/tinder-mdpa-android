package com.lluiscriado.tindermdpa.listeners;

/**
 * Created by lluis on 30/03/2018.
 */

public interface NewUserSeenListener {

    void onNewUserSeenRequestSuccess(boolean isMatch);

    void onNewUserSeenRequestError(String error);
}