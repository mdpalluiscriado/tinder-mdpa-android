package com.lluiscriado.tindermdpa.listeners;

/**
 * Created by lluis.criado on 06/03/2018.
 */

public interface LoginUserListener {

    void onLoginSuccess();

    void onLoginError(String error);
}