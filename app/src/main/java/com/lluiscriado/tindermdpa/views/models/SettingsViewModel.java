package com.lluiscriado.tindermdpa.views.models;

import com.lluiscriado.tindermdpa.listeners.UpdateValuesLoggedUserListener;
import com.lluiscriado.tindermdpa.manager.SettingsManager;
import com.lluiscriado.tindermdpa.manager.TinderManager;
import com.lluiscriado.tindermdpa.models.User;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by lluis.criado on 26/03/2018.
 */

@Singleton
public class SettingsViewModel {

    @Inject SettingsManager settingsManager;
    @Inject TinderManager tinderManager;


    // Public methods
    // =============================================================================================
    public User getLoggedUser() {
        return tinderManager.getLoggedUser();
    }

    public void updateValues(User newValuesForLoggedUser, UpdateValuesLoggedUserListener updateValuesLoggedUserListener) {
        settingsManager.updateValues(newValuesForLoggedUser, updateValuesLoggedUserListener);
    }

    public void logout() {
        tinderManager.setLoggedUser(null);
    }
}
