package com.lluiscriado.tindermdpa.views.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.lluiscriado.tindermdpa.views.fragments.CandidateFragment;
import com.lluiscriado.tindermdpa.views.fragments.ConversationsFragment;
import com.lluiscriado.tindermdpa.views.fragments.SettingsFragment;

/**
 * Created by lluis.criado on 31/01/2018.
 */

public class PageAdapter extends FragmentPagerAdapter {

    private int mNumOfTabs;

    public PageAdapter(FragmentManager fm, int numOfTabs) {
        super(fm);
        this.mNumOfTabs = numOfTabs;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                return new SettingsFragment();
            case 1:
                return new CandidateFragment();
            case 2:
                return new ConversationsFragment();
        }

        return null;
    }

    @Override
    public int getCount() {
        return mNumOfTabs;
    }
}