package com.lluiscriado.tindermdpa.views.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lluiscriado.tindermdpa.R;
import com.lluiscriado.tindermdpa.models.User;
import com.lluiscriado.tindermdpa.utils.DownloadImageTask;

import butterknife.BindView;
import butterknife.ButterKnife;
import toothpick.Scope;
import toothpick.Toothpick;

/**
 * Created by lluis.criado on 07/02/2018.
 */

public class CandidateDetailFragment extends Fragment {

    // Class properties
    // =============================================================================================
    @BindView(R.id.userImage)       ImageView userImageView;
    @BindView(R.id.currentJob)      TextView currentJob;
    @BindView(R.id.usernameTV)      TextView usernameTV;
    @BindView(R.id.descriptionTV)   TextView descriptionTV;

    public User candidate;

    // Fragment lifecycle methods
    // =============================================================================================
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Activity owner = getActivity();
        Scope scope = Toothpick.openScopes(owner.getApplication(), owner, this);
        super.onCreate(savedInstanceState);
        Toothpick.inject(this, scope);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_container_candidate_detail, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(candidate != null) customizeView();
    }

    // Private methods
    // =============================================================================================
    private void customizeView() {
        currentJob.setText(candidate.currentJob);
        descriptionTV.setText(candidate.description);
        usernameTV.setText(candidate.username);
        new DownloadImageTask(userImageView).execute(candidate.profilePicLink);
    }
}