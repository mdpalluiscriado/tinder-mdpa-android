package com.lluiscriado.tindermdpa.views.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.lluiscriado.tindermdpa.R;
import com.lluiscriado.tindermdpa.listeners.RegisterUserListener;
import com.lluiscriado.tindermdpa.views.SettingsActivity;
import com.lluiscriado.tindermdpa.views.models.WelcomeViewModel;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import toothpick.Scope;
import toothpick.Toothpick;

/**
 * Created by lluis on 31/01/2018.
 */

public class WelcomeRegisterFragment extends Fragment {

    // Class properties
    // =============================================================================================
    @Inject WelcomeViewModel welcomeViewModel;

    @BindView(R.id.username) EditText username;
    @BindView(R.id.password) EditText password;
    @BindView(R.id.checkBox) CheckBox checkBox;
    @BindView(R.id.textViewErrorMessage) TextView errorMessage;

    private View rootView;

    // Fragment lifecycle methods
    // =============================================================================================
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Activity owner = getActivity();
        Scope scope = Toothpick.openScopes(owner.getApplication(), owner, this);
        super.onCreate(savedInstanceState);
        Toothpick.inject(this, scope);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_welcome_register, container, false);
        ButterKnife.bind(this, rootView);
        errorMessage.setAlpha(0);
        return rootView;
    }

    // onClick methods
    // =============================================================================================
    @OnClick(R.id.registerButton)
    public void onRegisterButtonClicked() {

        welcomeViewModel.onRegisterUser(username.getText().toString(), password.getText().toString(), checkBox.isChecked(), new RegisterUserListener() {

            @Override
            public void onRegisterSuccess() {
                getActivity().runOnUiThread(() -> startActivity(new Intent(rootView.getContext(), SettingsActivity.class)));
            }

            @Override
            public void onRegisterError(String error) {
                getActivity().runOnUiThread(() -> {
                    if(error.equals("Wrong parameters")) {
                        errorMessage.setAlpha(1);
                    } else {
                        Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });
    }
}
