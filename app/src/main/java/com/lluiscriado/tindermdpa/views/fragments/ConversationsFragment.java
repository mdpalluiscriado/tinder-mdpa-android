package com.lluiscriado.tindermdpa.views.fragments;

import android.app.Activity;
import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.Toast;

import com.lluiscriado.tindermdpa.R;
import com.lluiscriado.tindermdpa.listeners.GetConversationsListener;
import com.lluiscriado.tindermdpa.listeners.GetMatchesListener;
import com.lluiscriado.tindermdpa.models.Conversation;
import com.lluiscriado.tindermdpa.models.User;
import com.lluiscriado.tindermdpa.views.adapters.ConversationAdapter;
import com.lluiscriado.tindermdpa.views.adapters.MatchesAdapter;
import com.lluiscriado.tindermdpa.views.models.ConversationsViewModel;

import java.util.List;
import java.util.Timer;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import toothpick.Scope;
import toothpick.Toothpick;

/**
 * Created by lluis.criado on 31/01/2018.
 */

public class ConversationsFragment extends Fragment {

    // Class properties
    // =============================================================================================
    @Inject ConversationsViewModel conversationsViewModel;

    @BindView(R.id.showMatchesButton)   FloatingActionButton fab;
    @BindView(R.id.swiperefresh)        SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.usersList)           ListView conversationsListView;

    private ConversationAdapter conversationAdapter;
    private List<Conversation> conversations;
    private Dialog matchesDialog;
    private View rootView;

    private Timer conversationsSchedulerTimer = null;


    // Fragment lifecycle methods
    // =============================================================================================
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Activity owner = getActivity();
        Scope scope = Toothpick.openScopes(owner.getApplication(), owner, this);
        super.onCreate(savedInstanceState);
        Toothpick.inject(this, scope);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_container_conversations, container, false);
        ButterKnife.bind(this, rootView);

        customizeView();

        return rootView;
    }

    // onClick methods
    // =============================================================================================
    @OnClick(R.id.showMatchesButton)
    void onShowMatchesButtonClicked() {
        matchesDialog = new Dialog(rootView.getContext());
        matchesDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        matchesDialog.setContentView(R.layout.dialog_matches);

        conversationsViewModel.getMatchList(new GetMatchesListener() {
            @Override
            public void onGetMatchesSuccess(List<User> matchesList) {
                getActivity().runOnUiThread(() -> {
                    GridView matchesGrid = matchesDialog.findViewById(R.id.grid);
                    populateGridView(matchesGrid, matchesList);
                    matchesDialog.show();
                });
            }

            @Override
            public void onGetMatchesError(String error) {
                getActivity().runOnUiThread(() -> Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show());
            }
        });
    }

    // Private methods
    // =============================================================================================
    private void customizeView() {
        swipeRefreshLayout.setOnRefreshListener(this::getConversations);

        conversationAdapter = new ConversationAdapter(rootView.getContext(), R.layout.cell_conversation);
        conversationsListView.setAdapter(conversationAdapter);
        conversationsListView.setOnItemClickListener((parent, view, position, id) -> openChatView(conversationAdapter.getItem(position)));

        getConversations();
    }

    private void getConversations() {
        conversationsViewModel.getConversationList(new GetConversationsListener() {
            @Override
            public void onGetConversationsSuccess(List<Conversation> conversationList) {
                getActivity().runOnUiThread(() -> {
                    conversations = conversationList;
                    conversationAdapter.clear();
                    conversationAdapter.addAll(conversations);

                    swipeRefreshLayout.setRefreshing(false);
                });
            }

            @Override
            public void onGetConversationsError(String error) {
                getActivity().runOnUiThread(() -> {
                    Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
                    swipeRefreshLayout.setRefreshing(false);
                });
            }
        });

        // TODO: Temporal code
        fab.show();
    }

    private void populateGridView(GridView matchesGrid, List<User> matchList) {
        final MatchesAdapter matchesAdapter = new MatchesAdapter(rootView.getContext(), R.layout.cell_grid_match);
        matchesAdapter.addAll(matchList);
        matchesGrid.setAdapter(matchesAdapter);
        matchesGrid.setOnItemClickListener((parent, view, position, id) -> {
            showChatFromUser(matchesAdapter.getItem(position));
            matchesDialog.hide();
        });
    }

    private void showChatFromUser(User user) {
        Conversation existingConversation = conversationsViewModel.isExistingConversation(conversations, user);

        if(existingConversation != null)
            openChatView(existingConversation);
        else
            openChatView(new Conversation(null, user, null));
    }

    private void openChatView(Conversation conversation) {
        ConversationsChatFragment conversationsChatFragment = new ConversationsChatFragment();
        conversationsChatFragment.conversation = conversation;


        android.support.v4.app.FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_conversations_layout, conversationsChatFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

        fab.hide();
    }
}
