package com.lluiscriado.tindermdpa.views.fragments;

import android.app.Activity;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lluiscriado.tindermdpa.R;

import butterknife.ButterKnife;
import toothpick.Scope;
import toothpick.Toothpick;

/**
 * Created by lluis on 02/04/2018.
 */

public class CandidateNoDataFragment extends Fragment {

    // Fragment lifecycle methods
    // =============================================================================================
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Activity owner = getActivity();
        Scope scope = Toothpick.openScopes(owner.getApplication(), owner, this);
        super.onCreate(savedInstanceState);
        Toothpick.inject(this, scope);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_no_data, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }
}
