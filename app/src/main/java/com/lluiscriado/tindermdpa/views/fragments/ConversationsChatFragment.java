package com.lluiscriado.tindermdpa.views.fragments;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.constraint.ConstraintLayout;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.lluiscriado.tindermdpa.R;
import com.lluiscriado.tindermdpa.listeners.GetMessagesListener;
import com.lluiscriado.tindermdpa.listeners.SendTextMessageListener;
import com.lluiscriado.tindermdpa.models.Conversation;
import com.lluiscriado.tindermdpa.models.Message;
import com.lluiscriado.tindermdpa.views.adapters.ChatArrayAdapter;
import com.lluiscriado.tindermdpa.views.models.ChatViewModel;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import toothpick.Scope;
import toothpick.Toothpick;

import static android.app.Activity.RESULT_OK;

/**
 * Created by lluis.criado on 30/01/2018.
 */

public class ConversationsChatFragment extends Fragment {

    // Class properties
    // =============================================================================================
    @Inject ChatViewModel chatViewModel;

    private static final int SELECT_PICTURE = 1;
    private static final int OPEN_CAMERA = 2;

    @BindView(R.id.adittionalControls)  ConstraintLayout additionalControls;
    @BindView(R.id.message_area)        EditText inputMessageEditText;
    @BindView(R.id.messages_list)       ListView messagesListView;

    public  Conversation conversation;
    private ChatArrayAdapter chatArrayAdapter;
    private View rootView;

    private Timer messagesSchedulerTimer = null;


    // Class constructor
    // =============================================================================================
    public ConversationsChatFragment() {}

    // Fragment lifecycle methods
    // =============================================================================================
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Activity owner = getActivity();
        Scope scope = Toothpick.openScopes(owner.getApplication(), owner, this);
        super.onCreate(savedInstanceState);
        Toothpick.inject(this, scope);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_container_conversations_chat, container, false);
        ButterKnife.bind(this, rootView);
        customizeView();
        setupListView();

        if(conversation.id != null)
            startSyncingWithServer();

        return rootView;
    }

    @Override
    public void onPause() {
        stopSyncingWithServer();
        super.onPause();
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            if (requestCode == SELECT_PICTURE || requestCode == OPEN_CAMERA)
                sendImageMessage(data.getData());
        }
    }

    // onClick methods
    // =============================================================================================
    @OnClick(R.id.imageButton)
    void openImageGallery() {
        Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_PICTURE);
    }

    @OnClick(R.id.cameraButton)
    void openCamera() {
        Intent intent = new Intent("android.media.action.IMAGE_CAPTURE");
        startActivityForResult(intent, OPEN_CAMERA);
    }

    void sendTextMessage(String text) {
        chatViewModel.sendTextMessage(conversation, text, new SendTextMessageListener() {
            @Override
            public void onSendTextMessageSuccess(Message message, String conversationId) {
                getActivity().runOnUiThread(() -> {
                    if(conversation.id == null) {
                        conversation.id = conversationId;
                        startSyncingWithServer();
                    }

                    chatArrayAdapter.add(message);
                    inputMessageEditText.setText("");
                });
            }
            @Override
            public void omSendTextMessageError(String error) {
                getActivity().runOnUiThread(() -> Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show());
            }
        });
    }

    void sendImageMessage(Uri data) {
        Log.i("TODO_METHOD", data.toString());
    }

    // Private methods
    // =============================================================================================
    private void customizeView() {
        ImageButton moreButton = rootView.findViewById(R.id.moreButton);
        moreButton.setOnClickListener(v -> switchAdditionalInputControls());

        ImageButton textButton = rootView.findViewById(R.id.textButton);
        textButton.setOnClickListener(v -> switchAdditionalInputControls());

        ImageView sendButton = rootView.findViewById(R.id.sendButton);
        sendButton.setOnClickListener(v -> sendTextMessage(inputMessageEditText.getText().toString()));
    }

    private void setupListView() {
        chatArrayAdapter = new ChatArrayAdapter(getActivity(), R.layout.cell_message_sender, conversation.user);
        messagesListView.setAdapter(chatArrayAdapter);
        messagesListView.setTranscriptMode(ListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
    }

    private void loadMessages() {
        chatViewModel.getMessages(conversation.id, new GetMessagesListener() {
            @Override
            public void onGetMessagesSuccess(List<Message> messageList) {
                getActivity().runOnUiThread(() -> {
                    if(messageList == null || messageList.isEmpty())  return;

                    chatArrayAdapter.clear();
                    chatArrayAdapter.addAll(messageList);
                });

            }

            @Override
            public void onGetMessagesError(String error) {
                getActivity().runOnUiThread(() -> Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show());
            }
        });
    }

    private void switchAdditionalInputControls() {
        additionalControls.setVisibility(additionalControls.getVisibility() == View.VISIBLE ? View.INVISIBLE : View.VISIBLE);
    }

    private void startSyncingWithServer() {
        stopSyncingWithServer();

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                loadMessages();
            }
        };
        messagesSchedulerTimer = new Timer();
        messagesSchedulerTimer.schedule(timerTask, 0, 5000);
    }

    private void stopSyncingWithServer() {
        if (messagesSchedulerTimer != null){
            messagesSchedulerTimer.cancel();
            messagesSchedulerTimer = null;
        }
    }
}
