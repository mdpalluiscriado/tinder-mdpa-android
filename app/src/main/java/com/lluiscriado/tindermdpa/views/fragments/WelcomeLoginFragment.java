package com.lluiscriado.tindermdpa.views.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.lluiscriado.tindermdpa.R;
import com.lluiscriado.tindermdpa.listeners.LoginUserListener;
import com.lluiscriado.tindermdpa.views.ContainerActivity;
import com.lluiscriado.tindermdpa.views.models.WelcomeViewModel;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import toothpick.Scope;
import toothpick.Toothpick;

/**
 * Created by lluis on 31/01/2018.
 */

public class WelcomeLoginFragment extends Fragment {

    // Class properties
    // =============================================================================================
    @Inject WelcomeViewModel welcomeViewModel;

    @BindView(R.id.username) EditText username;
    @BindView(R.id.password) EditText password;

    private View rootView;

    // Fragment lifecycle methods
    // =============================================================================================
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Activity owner = getActivity();
        Scope scope = Toothpick.openScopes(owner.getApplication(), owner, this);
        super.onCreate(savedInstanceState);
        Toothpick.inject(this, scope);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_welcome_login, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    // onClick methods
    // =============================================================================================
    @OnClick(R.id.loginButton)
    public void onLoginButtonClicked() {
        welcomeViewModel.onLoginUser(username.getText().toString(),
                password.getText().toString(),
                new LoginUserListener() {

            @Override
            public void onLoginSuccess() {
                getActivity().runOnUiThread(() -> startActivity(new Intent(rootView.getContext(), ContainerActivity.class)));
            }

            @Override
            public void onLoginError(String error) {
                getActivity().runOnUiThread(() -> Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show());
            }
        });
    }
}