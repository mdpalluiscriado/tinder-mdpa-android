package com.lluiscriado.tindermdpa.views.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.lluiscriado.tindermdpa.R;
import com.lluiscriado.tindermdpa.models.User;


/**
 * Created by lluis.criado on 01/02/2018.
 */

public class MatchesAdapter extends ArrayAdapter<User> {

    private int layoutResourceId;

    public MatchesAdapter(Context context, @LayoutRes int resource) {
        super(context, resource);
        layoutResourceId = resource;
    }

    private static class ViewHolder {
        TextView usernameTextView;
        ImageView profileImageView;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        // Get the data item for this position
        User match = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder = null;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(layoutResourceId, parent, false);

            viewHolder.usernameTextView = convertView.findViewById(R.id.grid_text);
            viewHolder.profileImageView = convertView.findViewById(R.id.grid_image);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.usernameTextView.setText(match.username);

        Glide.with(getContext())
                .load(match.profilePicLink)
                .apply(new RequestOptions().placeholder(R.drawable.default_profile))
                .apply(RequestOptions.circleCropTransform())
                .into((viewHolder.profileImageView));

        // Return the completed view to render on screen
        return convertView;
    }
}
