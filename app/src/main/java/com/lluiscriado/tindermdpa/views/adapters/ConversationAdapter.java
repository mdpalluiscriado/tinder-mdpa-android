package com.lluiscriado.tindermdpa.views.adapters;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.lluiscriado.tindermdpa.R;
import com.lluiscriado.tindermdpa.models.Conversation;
import com.lluiscriado.tindermdpa.utils.enums.MessageOwner;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by lluis.criado on 31/01/2018.
 */

public class ConversationAdapter extends ArrayAdapter<Conversation> {

    private int layoutResourceId;
    private Context mContext;

    private static class ViewHolder {
        TextView nameTextView;
        TextView messageTextView;
        TextView timeTextView;
        ImageView profileImageView;
    }

    public ConversationAdapter(@NonNull Context context, @LayoutRes int resource) {
        super(context, resource);
        mContext = context;
        layoutResourceId = resource;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        Conversation conversation = getItem(position);

        ViewHolder viewHolder;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(layoutResourceId, parent, false);

            viewHolder.nameTextView     = convertView.findViewById(R.id.name);
            viewHolder.messageTextView  = convertView.findViewById(R.id.message);
            viewHolder.timeTextView     = convertView.findViewById(R.id.time);
            viewHolder.profileImageView = convertView.findViewById(R.id.profileImage);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.nameTextView.setText(conversation.user.username);
        viewHolder.messageTextView.setText( conversation.lastMessage.getContentString() );

        if(!conversation.lastMessage.isRead && conversation.lastMessage.owner == MessageOwner.receiver) {
            viewHolder.nameTextView.setTypeface(Typeface.DEFAULT_BOLD);
            viewHolder.messageTextView.setTextColor(mContext.getResources().getColor(R.color.colorPrimary));
        } else {
            viewHolder.nameTextView.setTypeface(Typeface.DEFAULT);
            viewHolder.messageTextView.setTextColor(Color.DKGRAY);
        }

        Timestamp stamp = new Timestamp(conversation.lastMessage.timeStamp);
        SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
        Date messageDate = new Date(stamp.getTime());
        viewHolder.timeTextView.setText(dateFormat.format(messageDate));
        Glide.with(getContext())
                .load(conversation.user.profilePicLink)
                .apply(new RequestOptions().placeholder(R.drawable.default_profile))
                .apply(RequestOptions.circleCropTransform())
                .into(viewHolder.profileImageView);

        return convertView;
    }
}