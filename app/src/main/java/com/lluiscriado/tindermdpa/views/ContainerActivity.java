package com.lluiscriado.tindermdpa.views;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.lluiscriado.tindermdpa.R;
import com.lluiscriado.tindermdpa.views.adapters.PageAdapter;
import com.lluiscriado.tindermdpa.views.models.ConversationsViewModel;

import butterknife.ButterKnife;
import toothpick.Scope;
import toothpick.Toothpick;
import toothpick.config.Module;

/**
 * Created by lluis.criado on 01/02/2018.
 */

public class ContainerActivity extends AppCompatActivity {

    // Class properties
    // =============================================================================================
    public static String PACKAGE_NAME;

    // Activity lifecycle methods
    // =============================================================================================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Scope scope = Toothpick.openScopes(getApplication(), this);
        scope.installModules(new Module() {{
            bind(ConversationsViewModel.class);
        }});
        super.onCreate(savedInstanceState);
        Toothpick.inject(this, scope);

        setContentView(R.layout.activity_container);
        ButterKnife.bind(this);

        customizeView();
    }

    @Override
    protected void onDestroy() {
        Toothpick.closeScope(this);
        super.onDestroy();
    }

    // Private methods
    // =============================================================================================
    private void customizeView() {
        PACKAGE_NAME = getApplicationContext().getPackageName();

        ViewPager viewPager = findViewById(R.id.pager);
        viewPager.setAdapter(new PageAdapter(getSupportFragmentManager(), 3));

        TabLayout tabLayout = findViewById(R.id.tab_layout);
        tabLayout.setupWithViewPager(viewPager);

        tabLayout.getTabAt(0).setIcon(R.drawable.settings_icon);
        tabLayout.getTabAt(1).setIcon(R.drawable.ic_fire);
        tabLayout.getTabAt(2).setIcon(R.drawable.ic_chat);
    }
}
