package com.lluiscriado.tindermdpa.views.models;

import android.util.Log;

import com.lluiscriado.tindermdpa.listeners.GetCandidatesListener;
import com.lluiscriado.tindermdpa.listeners.NewUserSeenListener;
import com.lluiscriado.tindermdpa.manager.CandidateManager;
import com.lluiscriado.tindermdpa.manager.TinderManager;
import com.lluiscriado.tindermdpa.models.User;
import com.lluiscriado.tindermdpa.models.UserSeen;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by lluis on 05/02/2018.
 */

@Singleton
public class CandidateViewModel {

    @Inject CandidateManager candidateManager;
    @Inject TinderManager tinderManager;

    // Public methods
    // =============================================================================================
    public void getCandidateList(GetCandidatesListener getCandidatesListener) {
        candidateManager.getCandidateList(getCandidatesListener);
    }

    public void newUserSeenRequest(String userSeenId, boolean isLiked, NewUserSeenListener newUserSeenListener) {
        candidateManager.newUserSeen(userSeenId, isLiked, newUserSeenListener);
    }

    public List<User> applyFiltersTo(List<User> allCandidates) {
        List<User> filteredCandidates = new ArrayList<>();

        for (User candidate : allCandidates) {
            if(isLoggedUser(candidate) ||       // Checks if candidate is logged user
               isInUsersSeen(candidate) ||      // Checks if logged user has already seen the candidate
               isOutAgeRange(candidate) ||      // Checks if candidate age is inside logged user preferences
               isOutDistanceRange(candidate) || // Checks if candidate location is inside logged user preferences
               isGenderMatching(candidate))     // Checks if candidate gender matches with logged user preferences
            {
                Log.i("TINDER", "candidate discarted: " + candidate.userId);
            } else {
                filteredCandidates.add(candidate);
            }
        }

        return filteredCandidates;
    }

    // Private methods
    // =============================================================================================
    private boolean isLoggedUser(User candidate) {
        return candidate.userId.equals(tinderManager.getLoggedUser().userId);
    }

    private boolean isInUsersSeen(User candidate) {
        List<UserSeen> usersSeen = tinderManager.getLoggedUser().usersSeen;
        for (UserSeen userSeen: usersSeen) {
            if(candidate.userId.equals(userSeen.userSeenId))
                return true;
        }
        return false;
    }

    private boolean isOutAgeRange(User candidate) {
        // TODO: implement age range filter
        return false;
    }

    private boolean isOutDistanceRange(User candidate) {
        // TODO: implement distance filter
        return false;
    }

    private boolean isGenderMatching(User candidate) {
        // TODO: implement gender filter
        return false;
    }
}