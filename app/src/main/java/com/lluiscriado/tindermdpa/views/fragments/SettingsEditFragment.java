package com.lluiscriado.tindermdpa.views.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Toast;

import com.lluiscriado.tindermdpa.R;
import com.lluiscriado.tindermdpa.listeners.UpdateValuesLoggedUserListener;
import com.lluiscriado.tindermdpa.models.User;
import com.lluiscriado.tindermdpa.utils.DownloadImageTask;
import com.lluiscriado.tindermdpa.views.ContainerActivity;
import com.lluiscriado.tindermdpa.views.SettingsActivity;
import com.lluiscriado.tindermdpa.views.models.SettingsViewModel;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import toothpick.Scope;
import toothpick.Toothpick;

/**
 * Created by lluis on 08/02/2018.
 */

public class SettingsEditFragment extends Fragment {

    // Class properties
    // =============================================================================================
    @Inject SettingsViewModel settingsViewModel;

    @BindView(R.id.girlsRB)         RadioButton girlsRB;
    @BindView(R.id.boysRB)          RadioButton boysRB;
    @BindView(R.id.fromET)          EditText fromET;
    @BindView(R.id.toET)            EditText toET;
    @BindView(R.id.aboutMeET)       EditText aboutMeET;
    @BindView(R.id.currentJobET)    EditText currentJobET;
    @BindView(R.id.studiesET)       EditText studiesET;
    @BindView(R.id.profilePicIV)    ImageView profilePicIV;

    View rootView;

    // Fragment lifecycle methods
    // =============================================================================================
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Activity owner = getActivity();
        Scope scope = Toothpick.openScopes(owner.getApplication(), owner, this);
        super.onCreate(savedInstanceState);
        Toothpick.inject(this, scope);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_container_settings_edit, container, false);
        ButterKnife.bind(this, rootView);

        customizeView();

        return rootView;
    }

    // onClick methods
    // =============================================================================================
    @OnClick(R.id.saveButton)
    public void onSaveButtonClicked() {
        settingsViewModel.updateValues(getNewValues(), new UpdateValuesLoggedUserListener() {
            @Override
            public void onUpdateValuesLoggedUserSuccess() {
                if (getActivity().getClass().equals(SettingsActivity.class)) {
                    getActivity().runOnUiThread(() -> startActivity(new Intent(rootView.getContext(), ContainerActivity.class)));

                } else {
                    SettingsViewFragment settingsViewFragment = new SettingsViewFragment();

                    android.support.v4.app.FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
                    fragmentTransaction.replace(R.id.fragment_settings_layout, settingsViewFragment);
                    fragmentTransaction.addToBackStack(null);
                    fragmentTransaction.commit();
                }
            }

            @Override
            public void onUpdateValuesLoggedUserError(String error) {
                getActivity().runOnUiThread(() -> Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show());
            }
        });

    }

    private User getNewValues() {
        User loggedUser = settingsViewModel.getLoggedUser();

        if (loggedUser == null) return null;

        loggedUser.fromAge = Integer.parseInt(fromET.getText().toString());
        loggedUser.toAge = Integer.parseInt(toET.getText().toString());
        loggedUser.description = aboutMeET.getText().toString();
        loggedUser.currentJob = currentJobET.getText().toString();
        loggedUser.university = studiesET.getText().toString();

        return loggedUser;
    }

    // Private methods
    // =============================================================================================
    private void customizeView() {
        User loggedUser = settingsViewModel.getLoggedUser();
        assert loggedUser != null;

        if(loggedUser.isMale)  girlsRB.toggle();
        else                   boysRB.toggle();

        new DownloadImageTask(profilePicIV).execute(loggedUser.profilePicLink);

        fromET.setText(String.valueOf(loggedUser.fromAge));
        toET.setText(String.valueOf(loggedUser.toAge));
        aboutMeET.setText(loggedUser.description);
        currentJobET.setText(loggedUser.currentJob);
        studiesET.setText(loggedUser.university);
    }
}
