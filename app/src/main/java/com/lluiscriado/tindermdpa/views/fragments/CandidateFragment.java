package com.lluiscriado.tindermdpa.views.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.lluiscriado.tindermdpa.R;
import com.lluiscriado.tindermdpa.listeners.GetCandidatesListener;
import com.lluiscriado.tindermdpa.listeners.NewUserSeenListener;
import com.lluiscriado.tindermdpa.models.User;
import com.lluiscriado.tindermdpa.utils.Utils;
import com.lluiscriado.tindermdpa.utils.enums.AnimationType;
import com.lluiscriado.tindermdpa.views.models.CandidateViewModel;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import toothpick.Scope;
import toothpick.Toothpick;

/**
 * Created by lluis.criado on 31/01/2018.
 */

public class CandidateFragment extends android.support.v4.app.Fragment {

    @BindView(R.id.likeButton)      Button likeButton;
    @BindView(R.id.dislikeButton)   Button dislikeButton;

    @Inject CandidateViewModel candidateViewModel;

    // Class properties
    // =============================================================================================
    private Integer arrayIndex = 0;
    private List<User> candidates = new ArrayList<>();

    // Fragment lifecycle methods
    // =============================================================================================
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Activity owner = getActivity();
        Scope scope = Toothpick.openScopes(owner.getApplication(), owner, this);
        super.onCreate(savedInstanceState);
        Toothpick.inject(this, scope);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_container_candidate, container, false);
        ButterKnife.bind(this, rootView);
        customizeView();
        return rootView;
    }

    // onClick methods
    // =============================================================================================
    @OnClick(R.id.likeButton)
    void likeCandidate() {
        if(candidates.isEmpty() || arrayIndex >= candidates.size()) {
            noMoreCandidates();
            return;
        }

        candidateViewModel.newUserSeenRequest(candidates.get(arrayIndex).userId, true, new NewUserSeenListener() {
            @Override
            public void onNewUserSeenRequestSuccess(boolean isMatch) {
                getActivity().runOnUiThread(() -> {
                    if(isMatch)
                        Toast.makeText(getActivity(), "CONGRATULATIONS, NEW MATCH WITH " + candidates.get(arrayIndex).username, Toast.LENGTH_SHORT).show();

                    candidateLiked();
                });
            }

            @Override
            public void onNewUserSeenRequestError(String error) {
                getActivity().runOnUiThread(() -> Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show());
            }
        });
    }

    @OnClick(R.id.dislikeButton)
    void dislikeCandidate() {
        if(candidates.isEmpty() || arrayIndex >= candidates.size()) {
            noMoreCandidates();
            return;
        }

        candidateViewModel.newUserSeenRequest(candidates.get(arrayIndex).userId, false, new NewUserSeenListener() {
            @Override
            public void onNewUserSeenRequestSuccess(boolean isMatch) {
                candidateDisliked();
            }

            @Override
            public void onNewUserSeenRequestError(String error) {

            }
        });
    }

    // Private methods
    // =============================================================================================
    private void customizeView() {
        candidateViewModel.getCandidateList(new GetCandidatesListener() {
            @Override
            public void onGetCandidatesSuccess(List<User> allCandidates) {
                setCandidatesAndPresentFirst(allCandidates);
            }

            @Override
            public void onGetCandidatesError(String error) {
                Toast.makeText(getActivity(), error, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void candidateLiked() {
        arrayIndex ++;
        presentCandidate(AnimationType.leftToRight);
    }

    private void candidateDisliked() {
        arrayIndex ++;
        presentCandidate(AnimationType.rightToLeft);
    }

    private void setCandidatesAndPresentFirst(List<User> allCandidates) {
        this.candidates = candidateViewModel.applyFiltersTo(allCandidates);
        presentCandidate(AnimationType.noAnimation);
    }

    private void presentCandidate(AnimationType animationType) {
        if(candidates.isEmpty() || arrayIndex >= candidates.size()) {
            noMoreCandidates();
            return;
        }

        CandidatePresentFragment candidatePresentFragment = new CandidatePresentFragment();
        candidatePresentFragment.candidate = candidates.get(arrayIndex);
        Utils.presentFragment(candidatePresentFragment, animationType, (AppCompatActivity) getActivity(), R.id.fragment_candidate_layout);
    }

    private void noMoreCandidates() {
        likeButton.setVisibility(View.GONE);
        dislikeButton.setVisibility(View.GONE);
        CandidateNoDataFragment candidateNoDataFragment = new CandidateNoDataFragment();
        Utils.presentFragment(candidateNoDataFragment, AnimationType.noAnimation, (AppCompatActivity) getActivity(), R.id.fragment_candidate_layout);
    }
}