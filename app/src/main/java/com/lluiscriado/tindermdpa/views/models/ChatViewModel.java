package com.lluiscriado.tindermdpa.views.models;

import com.lluiscriado.tindermdpa.listeners.GetMessagesListener;
import com.lluiscriado.tindermdpa.listeners.SendTextMessageListener;
import com.lluiscriado.tindermdpa.manager.ChatManager;
import com.lluiscriado.tindermdpa.manager.TinderManager;
import com.lluiscriado.tindermdpa.models.Conversation;
import com.lluiscriado.tindermdpa.models.Message;
import com.lluiscriado.tindermdpa.utils.enums.MessageOwner;
import com.lluiscriado.tindermdpa.utils.enums.MessageType;

import javax.inject.Inject;

/**
 * Created by lluis on 31/03/2018.
 */

public class ChatViewModel {

    @Inject ChatManager chatManager;
    @Inject TinderManager tinderManager;

    public void sendTextMessage(Conversation conversation, String text, SendTextMessageListener sendTextMessageListener) {
        Message message = new Message(MessageType.text, text, tinderManager.getLoggedUser().userId, conversation.user.userId);
        message.owner = tinderManager.getLoggedUser().userId.equalsIgnoreCase(message.fromId) ? MessageOwner.sender : MessageOwner.receiver;


        if(conversation.id == null) {   // It's a first message for the conversation
            chatManager.sendTextMessageForNewConversation(conversation.user, message, sendTextMessageListener);
        } else {
            chatManager.sendTextMessageForExistingConversation(conversation, message, sendTextMessageListener);
        }
    }

    public void getMessages(String conversationId, GetMessagesListener getMessagesListener) {
        chatManager.getMessages(conversationId, getMessagesListener);
    }
}
