package com.lluiscriado.tindermdpa.views.fragments;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lluiscriado.tindermdpa.R;
import com.lluiscriado.tindermdpa.models.User;
import com.lluiscriado.tindermdpa.utils.DownloadImageTask;
import com.lluiscriado.tindermdpa.views.WelcomeActivity;
import com.lluiscriado.tindermdpa.views.models.SettingsViewModel;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import toothpick.Scope;
import toothpick.Toothpick;

/**
 * Created by lluis on 08/02/2018.
 */

public class SettingsViewFragment extends Fragment {

    // Class properties
    // =============================================================================================
    @Inject SettingsViewModel settingsViewModel;

    @BindView(R.id.profilePicture)  ImageView userImage;
    @BindView(R.id.textView)        TextView username;

    View rootView;
    // Fragment lifecycle methods
    // =============================================================================================
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Activity owner = getActivity();
        Scope scope = Toothpick.openScopes(owner.getApplication(), owner, this);
        super.onCreate(savedInstanceState);
        Toothpick.inject(this, scope);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_container_settings_view, container, false);
        ButterKnife.bind(this, rootView);
        customizeView();
        return rootView;
    }

    // onClick methods
    // =============================================================================================
    @OnClick(R.id.profilePicture)
    public void editProfile() {
        SettingsEditFragment settingsEditFragment = new SettingsEditFragment();

        android.support.v4.app.FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.fragment_settings_layout, settingsEditFragment);
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();
    }

    @OnClick(R.id.logoutButton)
    public void logout() {
        settingsViewModel.logout();
        getActivity().finish();
        startActivity(new Intent(rootView.getContext(), WelcomeActivity.class));
    }

    // Private methods
    // =============================================================================================
    private void customizeView() {
        User loggedUser = settingsViewModel.getLoggedUser();
        assert loggedUser != null;
        new DownloadImageTask(userImage).execute(loggedUser.profilePicLink);
        username.setText(loggedUser.username);
    }
}
