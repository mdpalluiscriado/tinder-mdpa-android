package com.lluiscriado.tindermdpa.views.models;

import com.lluiscriado.tindermdpa.listeners.LoginUserListener;
import com.lluiscriado.tindermdpa.listeners.RegisterUserListener;
import com.lluiscriado.tindermdpa.manager.WelcomeManager;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by lluis.criado on 01/03/2018.
 */

@Singleton
public class WelcomeViewModel {

    @Inject WelcomeManager welcomeManager;

    // Public methods
    // =============================================================================================
    public void onLoginUser(String username, String password, LoginUserListener loginUserListener) {
        if(username.isEmpty() || password.isEmpty()) {
            loginUserListener.onLoginError("Username and password must be filled");
            return;
        }

        welcomeManager.loginUser(username, password, loginUserListener);
    }

    public void onRegisterUser(String username, String password, Boolean isChecked, RegisterUserListener registerUserListener) {
        if (!isChecked || username.isEmpty() || password.isEmpty()) {
            registerUserListener.onRegisterError("Wrong parameters");
            return;
        }

        welcomeManager.registerUser(username, password, registerUserListener);
    }
}
