package com.lluiscriado.tindermdpa.views.models;

import com.lluiscriado.tindermdpa.listeners.GetConversationsListener;
import com.lluiscriado.tindermdpa.listeners.GetMatchesListener;
import com.lluiscriado.tindermdpa.manager.ConversationsManager;
import com.lluiscriado.tindermdpa.manager.TinderManager;
import com.lluiscriado.tindermdpa.models.Conversation;
import com.lluiscriado.tindermdpa.models.User;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

/**
 * Created by lluis.criado on 08/03/2018.
 */

@Singleton
public class ConversationsViewModel {

    @Inject ConversationsManager conversationsManager;
    @Inject TinderManager tinderManager;

    public void getConversationList(GetConversationsListener getConversationsListener) {
        conversationsManager.getConversationList(getConversationsListener);
    }

    public void getMatchList(GetMatchesListener getMatchesListener) {
        conversationsManager.getMatchList(getMatchesListener);
    }

    public Conversation isExistingConversation(List<Conversation> conversations, User user) {
        if(conversations != null && !conversations.isEmpty()) {
            for (Conversation conversationItem : conversations) {
                if(conversationItem.user.userId.equals(user.userId))
                    return conversationItem;
            }
        }
        return null;
    }
}