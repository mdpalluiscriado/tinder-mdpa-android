package com.lluiscriado.tindermdpa.views.fragments;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.lluiscriado.tindermdpa.R;
import com.lluiscriado.tindermdpa.models.User;
import com.lluiscriado.tindermdpa.utils.enums.AnimationType;
import com.lluiscriado.tindermdpa.utils.DownloadImageTask;
import com.lluiscriado.tindermdpa.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import toothpick.Scope;
import toothpick.Toothpick;

/**
 * Created by lluis on 12/02/2018.
 */

public class CandidatePresentFragment extends Fragment {

    // Class properties
    // =============================================================================================
    @BindView(R.id.imageView) ImageView imageView;
    @BindView(R.id.usernameTV) TextView username;

    public User candidate;

    // Fragment lifecycle methods
    // =============================================================================================
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        Activity owner = getActivity();
        Scope scope = Toothpick.openScopes(owner.getApplication(), owner, this);
        super.onCreate(savedInstanceState);
        Toothpick.inject(this, scope);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_container_candidate_present, container, false);
        ButterKnife.bind(this, rootView);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        if(candidate != null) customizeView();
    }

    // onClick methods
    // =============================================================================================
    @OnClick(R.id.imageView)
    public void showUserDetail() {
        CandidateDetailFragment candidateDetailFragment = new CandidateDetailFragment();
        candidateDetailFragment.candidate = candidate;

        Utils.presentFragment(
                candidateDetailFragment,
                AnimationType.bottomToTop,
                (AppCompatActivity) getActivity(),
                R.id.fragment_candidate_layout);
    }

    // Private methods
    // =============================================================================================
    private void customizeView() {
        new DownloadImageTask(imageView).execute(candidate.profilePicLink);
        username.setText(candidate.username);
    }
}