package com.lluiscriado.tindermdpa.views;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.lluiscriado.tindermdpa.R;
import com.lluiscriado.tindermdpa.utils.enums.AnimationType;
import com.lluiscriado.tindermdpa.utils.Utils;
import com.lluiscriado.tindermdpa.views.fragments.WelcomeLoginFragment;
import com.lluiscriado.tindermdpa.views.fragments.WelcomeRegisterFragment;
import com.lluiscriado.tindermdpa.views.models.WelcomeViewModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import toothpick.Scope;
import toothpick.Toothpick;
import toothpick.config.Module;

/**
 * Created by lluis.criado on 01/02/2018.
 */

public class WelcomeActivity extends AppCompatActivity {

    // Class properties
    // =============================================================================================
    @BindView(R.id.switchFragments) TextView switchFragments;

    private WelcomeLoginFragment welcomeLoginFragment;
    private WelcomeRegisterFragment welcomeRegisterFragment;

    // Activity lifecycle methods
    // =============================================================================================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Scope scope = Toothpick.openScopes(getApplication(), this);
        scope.installModules(new Module() {{
            bind(WelcomeViewModel.class);
        }});
        super.onCreate(savedInstanceState);
        Toothpick.inject(this, scope);

        setContentView(R.layout.activity_welcome);
        ButterKnife.bind(this);

        customizeView();
    }

    @Override
    protected void onDestroy() {
        Toothpick.closeScope(this);
        super.onDestroy();
    }

    // onClick methods
    // =============================================================================================
    @OnClick(R.id.switchFragments)
    public void onSwitchFragmentsClicked() {
        if(welcomeLoginFragment.isVisible()) {
            Utils.presentFragment(welcomeRegisterFragment, AnimationType.leftToRight, this, R.id.fragment_layout);
            switchFragments.setText("Login");
        } else {
            Utils.presentFragment(welcomeLoginFragment, AnimationType.rightToLeft, this, R.id.fragment_layout);
            switchFragments.setText("Create New Account");
        }
    }

    // Private methods
    // =============================================================================================
    private void customizeView() {
        welcomeLoginFragment = new WelcomeLoginFragment();
        welcomeRegisterFragment = new WelcomeRegisterFragment();

        Utils.presentFragment(welcomeLoginFragment, AnimationType.noAnimation, this, R.id.fragment_layout);
    }
}
