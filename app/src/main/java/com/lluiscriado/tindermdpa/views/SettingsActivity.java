package com.lluiscriado.tindermdpa.views;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.lluiscriado.tindermdpa.R;
import com.lluiscriado.tindermdpa.utils.Utils;
import com.lluiscriado.tindermdpa.utils.enums.AnimationType;
import com.lluiscriado.tindermdpa.views.fragments.SettingsEditFragment;
import com.lluiscriado.tindermdpa.views.models.SettingsViewModel;

import butterknife.ButterKnife;
import toothpick.Scope;
import toothpick.Toothpick;
import toothpick.config.Module;

/**
 * Created by lluis on 02/04/2018.
 */

public class SettingsActivity extends AppCompatActivity {

    // Activity lifecycle methods
    // =============================================================================================
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        Scope scope = Toothpick.openScopes(getApplication(), this);
        scope.installModules(new Module() {{
            bind(SettingsViewModel.class);
        }});
        super.onCreate(savedInstanceState);
        Toothpick.inject(this, scope);

        setContentView(R.layout.activity_settings);
        ButterKnife.bind(this);

        Utils.presentFragment(new SettingsEditFragment(), AnimationType.noAnimation, this, R.id.fragment_layout);
    }

    @Override
    protected void onDestroy() {
        Toothpick.closeScope(this);
        super.onDestroy();
    }
}
