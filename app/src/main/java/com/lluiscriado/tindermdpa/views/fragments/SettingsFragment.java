package com.lluiscriado.tindermdpa.views.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.lluiscriado.tindermdpa.R;
import com.lluiscriado.tindermdpa.utils.enums.AnimationType;
import com.lluiscriado.tindermdpa.utils.Utils;

/**
 * Created by lluis.criado on 31/01/2018.
 */

public class SettingsFragment extends Fragment {

    // Fragment lifecycle methods
    // =============================================================================================
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_container_settings, container, false);

        Utils.presentFragment(
                new SettingsViewFragment(),
                AnimationType.noAnimation,
                (AppCompatActivity) getActivity(),
                R.id.fragment_settings_layout);

        return rootView;
    }
}
