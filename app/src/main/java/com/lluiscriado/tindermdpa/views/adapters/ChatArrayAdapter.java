package com.lluiscriado.tindermdpa.views.adapters;

import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.constraint.ConstraintLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.lluiscriado.tindermdpa.R;
import com.lluiscriado.tindermdpa.models.Message;
import com.lluiscriado.tindermdpa.models.User;
import com.lluiscriado.tindermdpa.utils.enums.MessageOwner;


/**
 * Created by lluis.criado on 01/02/2018.
 */

public class ChatArrayAdapter extends ArrayAdapter<Message> {

    private User user;

    public ChatArrayAdapter(@NonNull Context context, @LayoutRes int resource, User user) {
        super(context, resource);
        this.user = user;
    }

    private static class ViewHolder {
        ImageView senderProfileImageView;

        TextView senderMessageTextView;
        TextView receiverMessageTextView;
    }

    @Override
    public int getItemViewType(int position) {
        return getItem(position).owner == MessageOwner.sender ? 0 : 1;
    }

    @Override
    public int getViewTypeCount() {
        return 2;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        final Message message = getItem(position);

        ViewHolder viewHolder;

        if ( message.owner == MessageOwner.receiver )
        {
            if (convertView == null) {
                viewHolder = new ViewHolder();
                LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(R.layout.cell_message_sender, parent, false);

                viewHolder.senderProfileImageView = convertView.findViewById(R.id.senderprofileimage);
                viewHolder.senderMessageTextView  = convertView.findViewById(R.id.sendermessage);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            final ConstraintLayout constraintLayout = (ConstraintLayout) convertView;
            caseSenderText(message, viewHolder, constraintLayout);

            Glide.with(getContext())
                    .load(user.profilePicLink)
                    .apply(new RequestOptions().placeholder(R.drawable.default_profile))
                    .apply(RequestOptions.circleCropTransform())
                    .into(viewHolder.senderProfileImageView);

        } else {
            if (convertView == null) {
                viewHolder = new ViewHolder();
                LayoutInflater inflater = LayoutInflater.from(getContext());
                convertView = inflater.inflate(R.layout.cell_message_receiver, parent, false);

                viewHolder.receiverMessageTextView = convertView.findViewById(R.id.receiver_message);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }

            caseReceiverText(message, viewHolder);
        }

        return convertView;
    }

    private void caseSenderText(Message message, ViewHolder viewHolder, ConstraintLayout constraintLayout) {
        viewHolder.senderMessageTextView.setVisibility(View.VISIBLE);
        viewHolder.senderMessageTextView.setText((String)message.content);

        constraintLayout.invalidate();
        constraintLayout.requestLayout();
    }

    private void caseReceiverText(Message message, ViewHolder viewHolder) {
        viewHolder.receiverMessageTextView.setVisibility(View.VISIBLE);
        viewHolder.receiverMessageTextView.setText((String)message.content);
    }
}
