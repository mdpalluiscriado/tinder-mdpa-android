package com.lluiscriado.tindermdpa.utils.enums;

/**
 * Created by lluis.criado on 31/01/2018.
 */

public enum MessageType {
    photo, text, location
}