package com.lluiscriado.tindermdpa.utils.enums;

/**
 * Created by lluis.criado on 26/03/2018.
 */

public enum RESTMethod {
    GET, POST
}
