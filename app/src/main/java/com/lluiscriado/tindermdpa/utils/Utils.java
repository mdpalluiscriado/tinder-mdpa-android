package com.lluiscriado.tindermdpa.utils;

import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.lluiscriado.tindermdpa.R;
import com.lluiscriado.tindermdpa.models.Message;
import com.lluiscriado.tindermdpa.models.User;
import com.lluiscriado.tindermdpa.models.UserSeen;
import com.lluiscriado.tindermdpa.utils.enums.AnimationType;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lluis on 01/02/2018.
 */

public class Utils {

    public static void presentFragment(Fragment fragment, AnimationType animationType, AppCompatActivity activity, int layoutId) {

        android.support.v4.app.FragmentTransaction fragmentTransaction = activity.getSupportFragmentManager().beginTransaction();

        switch (animationType) {
            case leftToRight:
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right);
                break;
            case rightToLeft:
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left);
                break;
            case bottomToTop:
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_bottom, R.anim.exit_to_top);
        }

        fragmentTransaction.replace(layoutId, fragment).commit();
    }

    public static List<String> jsonArrayToStringList(JSONArray jsonArray) {
        ArrayList<String> list = new ArrayList<>();
        if (jsonArray != null) {
            for (int i=0; i<jsonArray.length(); i++) {
                try {
                    list.add(jsonArray.getString(i));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return list;
    }

    public static List<User> jsonArrayToUserList(JSONArray jsonArray) {
        ArrayList<User> list = new ArrayList<>();
        if (jsonArray != null) {
            for (int i=0; i<jsonArray.length(); i++) {
                try {
                    list.add(new User(jsonArray.getJSONObject(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return list;
    }

    public static List<UserSeen> jsonArrayToUserSeenList(JSONArray jsonArray) {
        ArrayList<UserSeen> list = new ArrayList<>();
        if (jsonArray != null) {
            for (int i=0; i<jsonArray.length(); i++) {
                try {
                    list.add(new UserSeen(jsonArray.getJSONObject(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return list;
    }

    public static List<Message> jsonArrayToMessageList(JSONArray jsonArray) {
        ArrayList<Message> list = new ArrayList<>();
        if (jsonArray != null) {
            for (int i=0; i<jsonArray.length(); i++) {
                try {
                    list.add(new Message(jsonArray.getJSONObject(i)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }
        return list;
    }

}
