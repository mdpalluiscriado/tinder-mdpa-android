package com.lluiscriado.tindermdpa.utils.enums;

/**
 * Created by lluis.criado on 14/02/2018.
 */

public enum AnimationType {
    noAnimation, leftToRight, rightToLeft, bottomToTop
}